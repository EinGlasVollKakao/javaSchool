package net.einglasvollkakao.singleton;

public class Main {
    public static void main(String[] args) {
        var c1 = Counter.getInstance();
        var c2 = Counter.getInstance();

        System.out.println(c1.getCount());

        c1.increase();
        c1.increase();
        c1.increase();

        c2.decrease();

        System.out.println(c2.getCount());

        c2.increase();
        c2.increase();

        System.out.println(c1.getCount());
    }
}
