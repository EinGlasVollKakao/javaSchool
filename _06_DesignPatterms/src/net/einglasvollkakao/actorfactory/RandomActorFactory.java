package net.einglasvollkakao.actorfactory;

import net.einglasvollkakao.actorfactory.actors.Actor;
import net.einglasvollkakao.actorfactory.actors.Ant;
import net.einglasvollkakao.actorfactory.actors.Bee;
import net.einglasvollkakao.actorfactory.actors.Wolf;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomActorFactory {
    public static List<Actor> getActors(int count) {
        Random random = new Random();
        List<Actor> actors = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            switch (random.nextInt(0, 3)) {
                case 0 -> actors.add(new Bee("Marvin " + (i + 1)));
                case 1 -> actors.add(new Ant("Manuel " + (i + 1)));
                case 2 -> actors.add(new Wolf("Fabian " + (i + 1)));
            }
        }

        return actors;
    }
}
