package net.einglasvollkakao.actorfactory.actors;

public class Bee extends Actor {

    public Bee(String name) {
        super(name);
        type = "Bee";
    }
}
