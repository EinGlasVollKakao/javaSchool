package net.einglasvollkakao.actorfactory.actors;

public class Wolf extends Actor {

    public Wolf(String name) {
        super(name);
        type = "Wolf";
    }
}
