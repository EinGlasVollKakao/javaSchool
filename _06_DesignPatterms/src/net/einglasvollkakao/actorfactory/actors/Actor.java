package net.einglasvollkakao.actorfactory.actors;

public abstract class Actor {
    public String name;
    public String type;


    public Actor(String name) {
        this.name = name;
    }

    public void sayHello() {
        System.out.println("Name: " + name + " | Type: " + type);
    }

}
