package net.einglasvollkakao.actorfactory;

import net.einglasvollkakao.actorfactory.actors.Actor;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Actor> actors = RandomActorFactory.getActors(20);

        for (Actor actor : actors) {
            actor.sayHello();
        }
    }
}
